module.exports = {
  'src/**/*.{js,jsx}': 'eslint --fix',
  'src/**/*.scss': 'stylelint --fix',
};
