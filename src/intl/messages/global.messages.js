import { defineMessages } from 'react-intl';

export default defineMessages({
  OK: { id: 'global.ok', defaultMessage: 'OK' },
  CANCEL: { id: 'global.cancel', defaultMessage: 'Cancel' },
  SUCCESS: { id: 'global.success', defaultMessage: 'Success' },
});
