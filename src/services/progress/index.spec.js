import nprogress from 'nprogress';
import { decrementLoadingCount, incrementLoadingCount } from './index';

jest.mock('nprogress');

describe('services/progress', () => {
  it('should start and stop the counter if count is always one', () => {
    incrementLoadingCount();
    incrementLoadingCount();
    decrementLoadingCount();
    decrementLoadingCount();
    expect(nprogress.start).toHaveBeenCalledTimes(1);
    expect(nprogress.done).toHaveBeenCalledTimes(1);
  });
});
