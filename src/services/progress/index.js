import nprogress from 'nprogress';

let count = 0;

export const incrementLoadingCount = () => {
  if (count === 0) {
    nprogress.start();
  }

  count = Math.max(count + 1, 1);
};

export const decrementLoadingCount = () => {
  count = Math.max(count - 1, 0);

  if (count === 0) {
    nprogress.done();
  }
};
