import { getStoredLocale, storeLocale } from './localStorage';

describe('services/intl/localStorage', () => {
  it('should store and retrieve the same value', () => {
    const locale = 'Elvis Presley';
    storeLocale(locale);
    expect(getStoredLocale()).toBe(locale);
  });
});
