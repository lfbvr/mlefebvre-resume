import { getStoredLocale } from './localStorage';
import { getBrowserLocale } from './browser';

export * from './localStorage';
export * from './browser';

export const getDefaultLocale = () => getStoredLocale() || getBrowserLocale();
