import { getStoredLocale } from './localStorage';
import { getBrowserLocale } from './browser';
import { getDefaultLocale } from './index';

jest.mock('./localStorage');
jest.mock('./browser');

describe('services/intl', () => {
  describe('getDefaultLocale', () => {
    it('should return the stored locale if set', () => {
      getStoredLocale.mockImplementationOnce(() => 'test');
      expect(getDefaultLocale()).toBe('test');
    });
    it('should return the browser locale if not stored', () => {
      getStoredLocale.mockImplementationOnce(() => null);
      getBrowserLocale.mockImplementationOnce(() => 'test');
      expect(getDefaultLocale()).toBe('test');
    });
  });
});
