import { getBrowserLocale, setHtmlLangAttribute } from './browser';

describe('services/intl/browser', () => {
  describe('getBrowserLocale', () => {
    const makeNavigatorPropertyWritable = property => Object.defineProperty(navigator, property, {
      value: navigator[property],
      writable: true,
    });

    beforeAll(() => {
      makeNavigatorPropertyWritable('languages');
      makeNavigatorPropertyWritable('userLanguage');
      makeNavigatorPropertyWritable('language');
    });

    it('should work on latest browsers versions', () => {
      navigator.languages = ['fr_FR', 'en_US', 'pt_BR'];
      navigator.userLanguage = 'en_US';
      navigator.language = 'pt_BR';
      expect(getBrowserLocale())
        .toBe('fr');
    });

    it('should work on IE', () => {
      navigator.languages = [];
      navigator.userLanguage = 'en_US';
      navigator.language = 'pt_BR';
      expect(getBrowserLocale())
        .toBe('en');
    });

    it('should work on older browsers versions', () => {
      navigator.languages = [];
      navigator.userLanguage = '';
      navigator.language = 'pt_BR';
      expect(getBrowserLocale())
        .toBe('pt');
    });

    it('should return an empty string if nothing was found', () => {
      navigator.languages = [];
      navigator.userLanguage = '';
      navigator.language = '';
      expect(getBrowserLocale())
        .toBe('');
    });
  });

  describe('setHtmlLangAttribute', () => {
    it('should set the lang attribute of the html tag', () => {
      const html = document.getElementsByTagName('html')[0];
      html.setAttribute('lang', 'fr');
      setHtmlLangAttribute('en');

      expect(html.getAttribute('lang'))
        .toBe('en');
    });

    it('should return the same locale', () => {
      expect(setHtmlLangAttribute('en'))
        .toBe('en');
    });

    it('should return an empty string when the tag is not present', () => {
      const html = document.getElementsByTagName('html')[0];
      html.remove();
      expect(setHtmlLangAttribute('en'))
        .toBe('');
    });
  });
});
