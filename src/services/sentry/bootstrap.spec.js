import './bootstrap';
import config from 'config/env';
import * as Sentry from '@sentry/browser';

jest.mock('config/env');
jest.mock('@sentry/browser');

describe('services/sentry/bootstrap', () => {
  it('should init Sentry', () => {
    expect(Sentry.init).toHaveBeenCalledTimes(1);
    expect(Sentry.init).toHaveBeenCalledWith(config.sentry.options);
  });
});
