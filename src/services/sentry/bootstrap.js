import * as Sentry from '@sentry/browser';
import config from 'config/env';

Sentry.init({
  ...config.sentry.options,
});

export default Sentry;
