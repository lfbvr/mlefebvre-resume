import { removeClassname } from 'services/dom/css';
import { easeInOutQuad } from 'services/style/easing';
import { decrementLoadingCount } from 'services/progress';

import { cleanParticles } from './bootstrap';

const fadeOut = (t, content, container) => {
  content.style.zoom = `${1 + 10 * easeInOutQuad(t)}`;
  container.style.opacity = `${1 - (easeInOutQuad(t))}`;
};

const fadeOutAnimation = (delay = 1000, content, container, callback) => {
  let timer = 0;
  // Allow at least 20 steps of fading out
  const step = Math.min(100, delay / 20);
  let iid;

  const done = () => {
    clearInterval(iid);
    callback();
  };

  iid = setInterval(() => {
    if (timer > delay) {
      done();
      return;
    }
    fadeOut(timer / delay, content, container);
    timer += step;
  }, step);
};

export const destroy = (delay = 1000) => {
  const body = document.getElementsByTagName('body')[0];
  const splash = document.getElementById('splash');
  const content = document.getElementById('splash--content');

  const done = () => {
    cleanParticles();
    splash.remove();
    removeClassname(body, 'body--has-splash');
    decrementLoadingCount();
  };

  fadeOutAnimation(delay, content, splash, done);
};
