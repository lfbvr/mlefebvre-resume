import React, { memo } from 'react';

import classNames from './loading-screen.module.scss';

// Feel free to edit the loading screen.
const LoadingScreen = () => (
  <div className={classNames.container} />
);

LoadingScreen.displayName = 'LoadingScreen';

export default memo(LoadingScreen);
