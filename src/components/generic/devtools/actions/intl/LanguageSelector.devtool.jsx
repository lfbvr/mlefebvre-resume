import LanguageSelector from 'components/generic/intl/LanguageSelector';
import { makeManifest } from '../../core/manifest';
import { withRegisterDevTool } from '../../core/actions/register';

export default withRegisterDevTool(makeManifest({
  name: 'Language selector',
  render: LanguageSelector,
}));
