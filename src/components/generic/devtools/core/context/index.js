import React from 'react';

export const DevToolsContext = React.createContext({
  isEnabled: false,
  isLoaded: false,
});
