import React from 'react';
import PropTypes from 'prop-types';
import {
  compose,
  withPropsOnChange,
  withHandlers,
  withState,
} from 'recompose';
import {
  pick,
  uniqueId,
  omit,
  without,
} from 'lodash';

import { DevToolsContext } from '../index';

const contextProps = [
  'actions',
  'registerDevTool',
  'unregisterDevTool',
  'updateDevTool',
];

const enhancer = compose(
  // Map of actions (easier to handle than a list).
  withState('actions', 'onChangeActions', {}),
  // Maps are not ordered so keep the order of the actions to turn them in a map later.
  withState('actionsOrder', 'onChangeActionsOrder', []),

  withHandlers({
    // Generate a new dev tool action id and add the action to the map of registered actions.
    registerDevTool: ({
      actions,
      actionsOrder,
      onChangeActions,
      onChangeActionsOrder,
    }) => (manifest, props) => {
      const id = uniqueId('devTool');

      onChangeActions({
        ...actions,
        [id]: {
          id,
          manifest,
          props,
        },
      });

      onChangeActionsOrder([...actionsOrder, id]);

      return id;
    },

    // Update the current props of a dev tool action.
    updateDevTool: ({
      actions,
      onChangeActions,
    }) => (devToolId, props) => {
      if (actions[devToolId]) {
        onChangeActions({
          ...actions,
          [devToolId]: {
            ...actions[devToolId],
            props,
          },
        });
      } else {
        throw Error('A DevTool action tried to update without being registered');
      }
    },

    // Remove a dev tool action.
    unregisterDevTool: ({
      actions,
      onChangeActions,
      actionsOrder,
      onChangeActionsOrder,
    }) => (devToolId) => {
      onChangeActions(omit(actions, [devToolId]));
      onChangeActionsOrder(without(actionsOrder, devToolId));
    },
  }),

  /** Override actions to turn the map into a list. */
  withPropsOnChange(
    ['actions', 'actionsOrder'],
    ({ actions, actionsOrder }) => ({
      actions: actionsOrder.map(id => actions[id]),
    }),
  ),

  /** Change the reference of the context value only if there was a change in the actions. */
  withPropsOnChange(
    contextProps,
    props => ({
      context: pick(props, contextProps),
    }),
  ),
);

const DevToolsProvider = ({
  context,
  children,
}) => (
  <DevToolsContext.Provider value={context}>
    {children}
  </DevToolsContext.Provider>
);

DevToolsProvider.displayName = 'DevToolsProvider';

DevToolsProvider.propTypes = {
  /** Context value. */
  context: PropTypes.shape({
    registerDevTool: PropTypes.func.isRequired,
    unregisterDevTool: PropTypes.func.isRequired,
    updateDevTool: PropTypes.func.isRequired,
  }).isRequired,
  /** Provider children. */
  children: PropTypes.node,
};

DevToolsProvider.defaultProps = {
  children: null,
};

export default enhancer(DevToolsProvider);
