import React from 'react';

import { DevToolsContext } from './index';

const defaultMapContextToProps = context => ({ context });

export const withDevToolsContext
  = (mapContextToProps = defaultMapContextToProps) => WrappedComponent => props => (
    <DevToolsContext.Consumer>
      {context => (
        <WrappedComponent {...props} {...mapContextToProps(context)} />
      )}
    </DevToolsContext.Consumer>
  );
