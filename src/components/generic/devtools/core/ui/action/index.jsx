import React, { memo } from 'react';
import { compose } from 'recompose';

import { actionShape } from 'components/shapes/devtools';
import { getName } from '../../manifest';

import classNames from './action.module.scss';

const enhancer = compose(
  memo,
);

const DevTool = ({
  action: {
    id,
    props,
    manifest: {
      name,
      render: DevToolComponent,
    },
  },
}) => (
  <div className={classNames.action}>
    <h3 className={classNames.name}>{getName(name, props)}</h3>

    <DevToolComponent
      devToolId={id}
      {...props}
    />
  </div>
);

DevTool.displayName = 'DevTool';

DevTool.propTypes = {
  action: actionShape.isRequired,
};

export default enhancer(DevTool);
