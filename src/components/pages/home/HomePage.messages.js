import { defineMessages } from 'react-intl';

export default defineMessages({
  PAGE_TITLE: {
    id: 'views.home.title',
    defaultMessage: 'Home',
  },
});
