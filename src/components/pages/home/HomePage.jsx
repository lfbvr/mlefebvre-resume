import React, { memo } from 'react';
import { compose } from 'recompose';

const enhancer = compose(memo);

const Home = () => <div>Home</div>;

Home.displayName = 'Home';

Home.propTypes = {};

Home.defaultProps = {};

export default enhancer(Home);
