import { defineMessages } from 'react-intl';

export default defineMessages({
  PAGE_TITLE: {
    id: 'views.error.title',
    defaultMessage: '{hasCode, select, true {{code}} other {Error}}',
    description: "`code` is the error code, and `hasCode` is 'true' if `code` is defined.",
  },
});
