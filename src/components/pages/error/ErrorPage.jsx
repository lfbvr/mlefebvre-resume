import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';
import { Helmet } from 'react-helmet-async';

import FormattedMessageChildren from 'components/generic/intl/FormattedMessageChildren';
import ErrorPageTemplate from 'components/templates/error-page/ErrorPageTemplate';

import messages from './ErrorPage.messages';

const enhancer = compose(
  memo,
);

const ErrorPage = ({
  code,
}) => (
  <>
    <FormattedMessageChildren
      {...messages.PAGE_TITLE}
      values={{
        code,
        hasCode: code && 'true',
      }}
    >
      {title => (
        <Helmet>
          <title>{title}</title>
        </Helmet>
      )}
    </FormattedMessageChildren>

    <ErrorPageTemplate code={code} />
  </>
);

ErrorPage.propTypes = {
  code: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

ErrorPage.defaultProps = {
  code: null,
};

export default enhancer(ErrorPage);
