import { withProps } from 'recompose';

import ErrorPage from '../ErrorPage';

export default withProps(() => ({ code: '404' }))(ErrorPage);
