import PropTypes from 'prop-types';

// A valid React element prop.
export const elementShape = PropTypes.oneOfType([
  // Class component or functional component without memo / forwardRef.
  PropTypes.func,
  // Function component (with memo or forwardRef).
  PropTypes.shape({}),
  // Native element name.
  PropTypes.string,
]);
