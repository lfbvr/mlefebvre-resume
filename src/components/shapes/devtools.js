import PropTypes from 'prop-types';

import { elementShape } from './vendor';

export const manifestShape = PropTypes.shape({
  render: elementShape,
  mapProps: PropTypes.func,
  name: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.func,
  ]),
  group: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.func,
  ]),
});

export const actionShape = PropTypes.shape({
  id: PropTypes.string,
  manifest: manifestShape,
  props: PropTypes.shape(),
});
