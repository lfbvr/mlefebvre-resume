import { defineMessages } from 'react-intl';

export default defineMessages({
  OOPS: {
    id: 'pages.error.default.code',
    defaultMessage: 'Oops.',
    description: 'Default error code',
  },

  DEFAULT_MESSAGE: {
    id: 'pages.error.default.message',
    defaultMessage: 'Something went wrong',
    description: 'Default error message',
  },
});

export const errorMessages = defineMessages({
  404: {
    id: 'pages.error.404.message',
    defaultMessage: 'Not Found',
    description: 'HTTP 404 error code',
  },

  500: {
    id: 'pages.error.500.message',
    defaultMessage: 'Internal Server Error',
    description: 'HTTP 500 error code',
  },
});
