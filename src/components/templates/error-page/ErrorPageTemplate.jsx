import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';

import { FormattedMessage } from 'react-intl';

import messages, { errorMessages } from './ErrorPageTemplate.messages';

import classNames from './ErrorPageTemplate.module.scss';

const enhancer = compose(
  memo,
);

const ErrorPageTemplate = ({ code }) => (
  <div className={classNames.container}>
    <h1 className={classNames.code}>
      {code || <FormattedMessage {...messages.OOPS} />}
    </h1>

    <div className={classNames.description}>
      <FormattedMessage {...(errorMessages[code] || messages.DEFAULT_MESSAGE)} />
    </div>
  </div>
);

ErrorPageTemplate.propTypes = {
  code: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

ErrorPageTemplate.defaultProps = {
  code: null,
};

export default enhancer(ErrorPageTemplate);
