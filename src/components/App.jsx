import React, { Suspense, lazy } from 'react';
import { lifecycle, compose } from 'recompose';
import { Provider as ReduxProvider } from 'react-redux';
import { HelmetProvider } from 'react-helmet-async';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom';

import store from 'redux/store';
import { destroy } from 'services/splash';

import { THEME } from 'config/style';

import IntlProvider from './generic/intl/IntlProvider';
import LoadingScreen from './generic/loading/LoadingScreen';
import DevToolsProvider from './generic/devtools/core/context/provider/lazy';
import DevToolsUi from './generic/devtools/core/ui';
import LoadDevTool from './generic/devtools/core/actions/load/LoadDevTool';

import routes from './routes';

const Home = lazy(() => import('./pages/home/HomePage'));
const NotFound = lazy(() => import('./pages/error/not-found/NotFoundPage'));

const enhancer = compose(
  lifecycle({
    /** Hide splash screen when app is mounted. */
    componentDidMount() {
      destroy(THEME.splash.hideDelay);
    },
  }),
);

const App = () => (
  <HelmetProvider>
    <ReduxProvider store={store}>
      <IntlProvider>
        <Router>
          <Suspense fallback={<LoadingScreen />}>
            <DevToolsProvider>
              <Switch>
                <Route
                  path={routes.HOME.ROOT}
                  exact
                  component={Home}
                />

                <Route component={NotFound} />

              </Switch>

              <DevToolsUi />
              <LoadDevTool name="intl/LanguageSelector" />
            </DevToolsProvider>
          </Suspense>
        </Router>
      </IntlProvider>
    </ReduxProvider>
  </HelmetProvider>
);

export default enhancer(App);
