import makeLocalStorageAdapter from './localStorageAdapter';

describe('utils/storage/localStorage', () => {
  describe('makeLocalStorageAdapter', () => {
    it('should create an adapter', () => {
      const adapter = makeLocalStorageAdapter('TEST');

      adapter.set('TEST_VALUE');
      expect(adapter.key !== 'TEST').toBe(true);
      expect(adapter.get()).toBe('TEST_VALUE');
      expect(localStorage.getItem(adapter.key)).toBe('TEST_VALUE');
    });
  });
});
