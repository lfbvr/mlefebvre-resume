import Sentry from 'services/sentry/bootstrap';

import bootstrapParticles from 'services/splash/bootstrap';
import 'nprogress/nprogress.css';

bootstrapParticles();

setTimeout(() => {
  import('./app')
    .catch((err) => {
      Sentry.captureException(err);
      if (process.env.NODE_ENV !== 'production') {
        // eslint-disable-next-line no-console
        console.error(err);
      }
    });
}, 1000);
