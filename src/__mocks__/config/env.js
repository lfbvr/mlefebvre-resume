export default {
  app: {
    version: '1.0.0',
  },

  sentry: {
    options: {
      dsn: 'sentry_dsn',
      environment: 'sentry_environment',
      release: 'sentry_release',
    },
  },
};
