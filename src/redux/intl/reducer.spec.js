import reducer from './reducer';

import {
  SET_LOCALE_SUCCESS,
} from './actions';

describe('redux/intl/reducer', () => {
  describe('locale', () => {
    it('sets the locale', () => {
      const locale = 'fr';
      const action = SET_LOCALE_SUCCESS({ locale, messages: {} });
      const state = reducer(undefined, action);

      expect(state.locale).toBe(locale);
    });
  });
});
