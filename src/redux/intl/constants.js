export const ACTIONS = {
  SET_LOCALE_LOADING: 'intl/setLocale:loading',
  SET_LOCALE_SUCCESS: 'intl/setLocale:success',
  SET_LOCALE_FAILURE: 'intl/setLocale:failure',
};
