import React, { memo } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import classNames from './Title.module.scss';

const TITLE_LEVELS = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'];

export const TitleBase = ({
  children,

  level,
  className,

  light,
}) => React.createElement(
  level,
  {
    className: cn(
      classNames.title,
      light && classNames.light,
      className,
    ),
  },
  children,
);

TitleBase.displayName = 'TitleBase';

TitleBase.propTypes = {
  /** The title to display. */
  children: PropTypes.node,
  /** The title level to use. */
  level: PropTypes.oneOf(TITLE_LEVELS),
  /** Custom className. */
  className: PropTypes.string,
  /** Use light title. */
  light: PropTypes.bool,
};

TitleBase.defaultProps = {
  children: '',
  level: 'h1',
  className: '',
  light: false,
};

const Title = memo(TitleBase);

Title.TITLE_LEVELS = TITLE_LEVELS;

export default Title;
