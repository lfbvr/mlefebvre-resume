import React, { memo } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'recompose';

import { elementShape } from 'components/shapes/vendor';

import { Link as ReactRouterLink } from 'react-router-dom';

const enhancer = compose(
  memo,
);

/* A Link that can be disabled. */
export const LinkBase = ({
  isDisabled,
  disabledElement,
  children,
  ...props
}) => React.createElement(
  isDisabled ? disabledElement : ReactRouterLink,
  props,
  children,
);

LinkBase.displayName = 'LinkProxy';

LinkBase.propTypes = {
  isDisabled: PropTypes.bool,
  /** HTML element instead of `<a>` when the link is disabled. */
  disabledElement: elementShape,
  children: PropTypes.node,
};

LinkBase.defaultProps = {
  isDisabled: false,
  disabledElement: 'div',
  children: null,
};

export default enhancer(LinkBase);
