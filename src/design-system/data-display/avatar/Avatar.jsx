import React, { memo, useMemo } from 'react';
import {
  arrayOf, oneOfType, string, oneOf,
} from 'prop-types';
import cn from 'classnames';
import defaultAvatar from 'style/assets/avatar/default_avatar.png';
import { makeBackgroundImageProperty } from './utils';

import classNames from './Avatar.module.scss';

const AVATAR_SIZES = {
  DEFAULT: 'default',
  SMALL: 'small',
};

export const AvatarBase = ({
  className,
  size,
  src,
}) => {
  const avatarStyle = useMemo(
    () => ({
      backgroundImage: makeBackgroundImageProperty(src, defaultAvatar),
    }),
    [src],
  );

  return (
    <div
      className={cn(
        className,
        classNames.avatar,
        classNames[size],
      )}
      style={avatarStyle}
    />
  );
};

AvatarBase.displayName = 'AvatarBase';

AvatarBase.propTypes = {
  /** Avatar image(s). */
  src: oneOfType([string, arrayOf(string)]),
  /** Root element className. */
  className: string,
  /** Specifies avatar size, see `AVATAR_SIZES`. */
  size: oneOf(Object.values(AVATAR_SIZES)),
};

AvatarBase.defaultProps = {
  src: null,
  className: null,
  size: AVATAR_SIZES.DEFAULT,
};

const Avatar = memo(AvatarBase);

Avatar.AVATAR_SIZES = AVATAR_SIZES;

export default Avatar;
