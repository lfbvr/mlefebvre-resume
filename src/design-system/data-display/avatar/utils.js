import { flatten } from 'lodash';

export const makeBackgroundImageProperty = (...urls) => (
  flatten(urls)
    .filter(Boolean)
    .map(url => `url(${url})`)
    .join(', ')
);
