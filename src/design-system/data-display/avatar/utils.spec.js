import { makeBackgroundImageProperty } from './utils';

describe('components/atoms/Avatar/utils.js', () => {
  describe('makeBackgroundImageProperty', () => {
    it('should return an empty string if no args are provided', () => {
      expect(makeBackgroundImageProperty())
        .toBe('');
    });
    it('should return a list of urls separated by commas', () => {
      expect(makeBackgroundImageProperty('a', 'b'))
        .toBe('url(a), url(b)');
    });
    it('should flatten array args', () => {
      expect(makeBackgroundImageProperty(['a', 'b'], 'c'))
        .toBe('url(a), url(b), url(c)');
    });
    it('should filter out falsy values', () => {
      expect(makeBackgroundImageProperty(['a', 'b'], undefined, null, 0, NaN, '', false, 'c'))
        .toBe('url(a), url(b), url(c)');
    });
  });
});
