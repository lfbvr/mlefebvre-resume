import { BREAKPOINTS_MAP, THEME } from './style';

jest.mock('style/config.variables.scss', () => ({
  breakpoints: {
    justin: '100px',
    ariana: '200px',
    michael: '300px',
  },
}));

jest.mock('style/theme.variables.scss', () => ({
  justin: 'red',
  ariana: 'blue',
}));

describe('config/style', () => {
  describe('BREAKPOINTS', () => {
    it('should be a map of the breakpoints defined in config.variables.scss', () => {
      expect(BREAKPOINTS_MAP)
        .toMatchObject({
          justin: 100,
          ariana: 200,
          michael: 300,
        });
    });
  });

  describe('THEME', () => {
    it('should be a map of the theme variables', () => {
      expect(THEME)
        .toMatchObject({
          justin: 'red',
          ariana: 'blue',
        });
    });
  });
});
