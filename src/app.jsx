import * as Sentry from '@sentry/browser';
import React from 'react';
import ReactDOM from 'react-dom';

import App from 'components/App';
import { setLocale } from 'redux/intl/actions';
import store from 'redux/store';

import './style/main.scss';

(async function main() {
  await store.dispatch(setLocale());
  ReactDOM.render(
    <App />,
    document.getElementById('app'),
  );
}())
  .catch((err) => {
    Sentry.captureException(err);
    if (process.env.NODE_ENV !== 'production') {
      // eslint-disable-next-line no-console
      console.error(err);
    }
  });
