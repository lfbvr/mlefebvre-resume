FROM nginx:stable

# users are not allowed to listen on priviliged ports
COPY .nginx-storybook/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 8080

# App will not run as root, ajust permissions of specific folders.
RUN addgroup nginx root \
  # Place pid in /var/run/nginx/nginx.pid instead of /var/run/nginx.pid
  && mkdir /run/nginx \
  && touch /run/nginx/nginx.pid \
  && sed -i "s/pid.*/pid        \/var\/run\/nginx\/nginx.pid;/g"  /etc/nginx/nginx.conf \
  && chmod -R g+rwx /var/cache/nginx /var/log/nginx /run/nginx /etc/nginx/conf.d \
  # Put your htpasswd here.
  && mkdir /nginx-auth

USER nginx

COPY storybook-static /usr/share/nginx/html
