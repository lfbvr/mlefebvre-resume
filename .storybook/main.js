module.exports = {
  stories: [
    '../src/components/**/*.stories.{js,jsx,mdx}',
    '../src/design-system/**/*.stories.{js,jsx,mdx}',
    '../src/style/**/*.stories.{jsx,jsx,mdx}',
  ],

  addons: [
    '@storybook/addon-docs',
    '@storybook/addon-actions',
    '@storybook/addon-knobs',
    '@storybook/addon-viewport',
    'storybook-addon-intl',
  ],
};
